
-- lua 5.4
local _a <const> = ('a'):byte(1)
local _z <const> = ('z'):byte(1)
local _A <const> = ('A'):byte(1)
local _Z <const> = ('Z'):byte(1)
local _0 <const> = ('0'):byte(1)
local _9 <const> = ('9'):byte(1)


function gerar_senha(min, max, r, mi, ma, di)
    if #r <= max then
        if #r >= min and mi and ma and di then
            --print(r)
        end
	if max - #r > 1 or ma or di then
		for c = _a,_z do
		    gerar_senha(min, max, r .. string.char(c), true, ma, di)
		end
	end
	if max - #r > 1 or mi or di then
		for c = _A,_Z do
		    gerar_senha(min, max, r .. string.char(c), mi, true, di)
		end
	end
	if max - #r > 1 or mi or ma then
		for c = _0,_9 do
		    gerar_senha(min, max, r .. string.char(c), mi, ma, true)
		end
	end
    end
end

print('senha_1')
print('gerar_senha(3,4,\'\',false,false,false)')
gerar_senha(3,4,'',false,false,false)

-- time lua5.3 senha_string.lua

-- real    4m15.225s
-- user    4m15.216s
-- sys     0m0.004s
