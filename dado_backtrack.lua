

function gerar_dados(n, m, d, r)
    if d == n then
        io.write("{")
        for i = 1, n do
            if i > 1 then
                io.write(",")
            end
            io.write(r[i])
        end
        print("}")
    else
        for i = 1, m+1 do
            r[d+1] = i
            gerar_dados(n, m, d + 1, r)
        end
    end
end


function combinacao(a, n, k, d, r, u)
    if d == k then
        io.write("{")
        for i = 1, d do
            if i > 1 then
                io.write(",")
            end
            io.write(a[r[i]])
        end
        io.write("}")
    else
        for i = 1, n do
            if not u[i] and (d == 0 or r[d] < i) then
                r[d+1] = i
                u[i] = true
                combinacao(a, n, k, d + 1, r, u)
                u[i] = false
            end
        end
    end
end

local n = 26
local m = 12
local k = 12 -- 5!/3!/2! = 10

local r = {}
local u = {}
local a = {}
for i = 1, n+1 do
    r[i] = nil
    u[i] = nil
    a[i] = i
end



combinacao(a, n, k, 0, r, u)
