
-- lua 5.3

MIN_MINUSCULAS = 1
MIN_MAISCULAS = 1
MIN_DIGITOS = 1
function e_valido(min, t, r)
    if #r < min then
        return false
    end
    if t.m < MIN_MINUSCULAS then
        return false
    elseif t.M < MIN_MAISCULAS then
        return false
    elseif t.d < MIN_DIGITOS then
        return false
    else
        return true
    end
end



function gerar_senha(min, max, t, r)
    if t == nil and r == nil then
        return gerar_senha(min, max, {m=0, M=0, d=0}, '')
    end
    if #r <= max then
        if e_valido(min, t, r) then
            --print(r)
        end
        for c = ('a'):byte(1),('z'):byte(1) do
            t.m = t.m + 1
            gerar_senha(min, max, t, r .. string.char(c))
            t.m = t.m - 1
        end
        for c = ('A'):byte(1),('Z'):byte(1) do
            t.M = t.M + 1
            gerar_senha(min, max, t, r .. string.char(c))
            t.M = t.M - 1
        end
        for c = ('0'):byte(1),('9'):byte(1) do
            t.d = t.d + 1
            gerar_senha(min, max, t, r .. string.char(c))
            t.d = t.d - 1
        end
    end
end


gerar_senha(3,4)

