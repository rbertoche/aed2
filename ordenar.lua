

function ordenar(a, n)
    -- Insertionsort

    if n == 0 then
        return
    end
    ordenar(a,n-1)
    local i = n
    while i > 1 and a[i-1] > a[i] do
        a[i], a[i-1] = a[i-1], a[i]
        i = i - 1
    end
end

