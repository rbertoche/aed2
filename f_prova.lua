
function f(a, b, x, y)
    if x == 0 or y == 0 then
        return x + y
    end
    local c = 1
    if a[x] == b[y] then
        c = 0
    end
    local ret = math.min(f(a, b, x-1, y-1) + c,
                         f(a, b, x-1, y) + 1,
                         f(a, b, x, y-1) + 1)
    return ret
end