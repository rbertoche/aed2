
-- lua 5.3

B = require 'buffer'


MIN_MINUSCULAS = 1
MIN_MAISCULAS = 1
MIN_DIGITOS = 1
function e_valido(min, t, r)
    if #r < min then
        return false
    end
    if t.m < MIN_MINUSCULAS then
        return false
    elseif t.M < MIN_MAISCULAS then
        return false
    elseif t.d < MIN_DIGITOS then
        return false
    else
        return true
    end
end


letras = {}
for c = ('a'):byte(1),('z'):byte(1) do
    table.insert(letras, {'m', c})
end
for c = ('A'):byte(1),('Z'):byte(1) do
    table.insert(letras, {'M', c})
end
for c = ('0'):byte(1),('9'):byte(1) do
    table.insert(letras, {'d', c})
end

function gerar_senha(min, max, t, r)
    if t == nil and r == nil then
        return gerar_senha(min, max, {m=0, M=0, d=0}, B'')
    end
    if #r <= max then
        if e_valido(min, t, r) then
            --print(r)
        end
        for _, value in pairs(letras) do
            key = value[1]
            c = value[2]
            r:insert(string.char(c))
            t[key] = t[key] + 1
            gerar_senha(min, max, t, r)
            t[key] = t[key] - 1
            r:setlen(#r - 1)
        end
    end
end


gerar_senha(3,4)

