
-- existe uam forma de fazer media ponderada sem fazer parametrizacao/generalizacao


function buscabinaria(l, i, n, x)
    local m = (i + n)//2
    if n - i + 1 == 0 then
        return 0
    elseif l[m] == x then
        return m
    elseif l[m] > x then
        return buscabinaria(l, i, m - 1, x)
    else
        return buscabinaria(l, m + 1, n, x)
    end
end