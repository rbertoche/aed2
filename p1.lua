

function maior(a, n)
    -- assume n >= 1
    if n == 1 then
        return 1
    else
        local i = maior(a, n-1)
        if a[i] > a[n] then
            return i
        else
            return n
        end
    end
end

function somaAlternante(a,n)
    -- n >= 1
    if n == 1 then
        return a[1]
    elseif n % 2 == 1 then
        return a[n] + somaAlternante(a, n-1)
    else
        return -a[n] + somaAlternante(a, n-1)
    end
end

function saoDistintos(a, n)
    if n <= 1 then
        return true
    end
    for i=1,n-1 do
        if a[n] == a[i] then
            return false
        end
    end
    return saoDistintos(a, n-1)
end

function norma(v, n)
    -- n >= 1
    if n == 1 then
        return v[n]
    else
        return math.sqrt(math.pow(norma(v, n-1), 2) +
                         math.pow(v[n], 2))
    end
end