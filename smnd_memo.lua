
function smnd(a, n)
    local r = 0
    for i = 1,n do
        r = math.max(r, smnd_t(a, i))
    end
    return r
end

function smnd_t(a, n_)
    local t = {}
    t[0] = 0
    for n = 1,n_ do
        if n > 0 then
            local r = 1
            for i = 1,n-1 do
                if a[i] <= a[n] then
                    r = math.max(t[i] + 1, r)
                end
            end
            t[n] = r
        else
            return 0
        end
    end
    return t[n_]
end

print(smnd({1, 2, 3}, 0))