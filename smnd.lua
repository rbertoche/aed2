


function smnd(a, n)
    local r = 0
    for i = 1,n do
        r = math.max(r, smnd_t(a, i))
    end
    return r
end

function smnd_t(a, n)
    if n > 0 then
        local i
        local r = 1
        for i = 1,n-1 do
            if a[i] <= a[n] then
                r = math.max(smnd_t(a,i) + 1, r)
            end
        end
        return r
    else
        return 0
    end
end

print(smnd({1, 2, 3}, 3))