

function permutacao(a, n, d, r, u)
    if d == n then
        io.write("{")
        for i = 1, n do
            if i > 1 then
                io.write(",")
            end
            io.write(a[r[i]])
        end
        io.write("}")
    else
        for i = 1, n do
            if not u[i] then
                r[d+1] = i
                u[i] = true
                permutacao(a, n, d + 1, r, u)
                u[i] = false
            end
        end
    end
end




function permutacao_circular(a, n, d, r, u)
    if d == n then
        io.write("{")
        for i = 1, n do
            if i > 1 then
                io.write(",")
            end
            io.write(a[r[i]])
        end
        io.write("}")
    else
        for i = 1, n do
            if not u[i] then
                r[d+1] = i
                u[i] = true
                permutacao_circular(a, n, d + 1, r, u)
                u[i] = false
            end
        end
    end
end
