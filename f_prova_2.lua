
mt = {}
for i=0,20 do
    mt[i] = {}
end

function f(a, b, x, y)
    if mt[x][y] ~= nil then
        return mt[x][y]
    end
    if x == 0 or y == 0 then
        mt[x][y] = x + y
        return x + y
    end
    local c = 1
    if a[x] == b[y] then
        c = 0
    end
    local ret = math.min(f(a, b, x-1, y-1) + c,
                         f(a, b, x-1, y) + 1,
                         f(a, b, x, y-1) + 1)
    mt[x][y] = ret
    return ret
end