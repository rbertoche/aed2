# coding: utf8

from numpy import array

mem = [-1 for i in range(10000)] 

def fi(n):
    if mem[n] == -1:
        if n == 1:
            mem[n] = 1
            return 1
        elif n == 2:
            mem[n] = 1
            return 1
        v = fi(n-1) + fi(n-2)
        mem[n] = v
        return v
    else:
        return mem[n]




# ma(a, n-1) * (n-1)/n + A[n]/n