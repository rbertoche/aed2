
-- lua 5.3

B = require 'buffer'
function e_valido(min, d, r)
    local maiusculas = 0
    local minusculas = 0
    local digitos = 0
    if #r < min then
        return false
    end
    for _,c in r:ipairs() do
        if ('a'):byte(1) <= c and c <= ('z'):byte(1) then
            minusculas = minusculas + 1
        elseif ('A'):byte(1) <= c and c <= ('Z'):byte(1) then
            maiusculas = maiusculas + 1
        elseif ('0'):byte(1) <= c and c <= ('9'):byte(1) then
            digitos = digitos + 1
        end
    end
    MIN_MINUSCULAS = 1
    MIN_MAISCULAS = 1
    MIN_DIGITOS = 1
    if minusculas < MIN_MINUSCULAS then
        return false
    elseif maiusculas < MIN_MAISCULAS then
        return false
    elseif digitos < MIN_DIGITOS then
        return false
    else
        return true
    end
end

letras = {}
for c = ('a'):byte(1),('z'):byte(1) do
    table.insert(letras, c)
end
for c = ('A'):byte(1),('Z'):byte(1) do
    table.insert(letras, c)
end
for c = ('0'):byte(1),('9'):byte(1) do
    table.insert(letras, c)
end

function gerar_senha(min, max, d, r)
    assert(d == #r)
    if d <= max then
        if e_valido(min, d, r) then
            --print(r)
        end
        for _, c in pairs(letras) do
            r:insert(string.char(c))
            gerar_senha(min, max, d + 1, r)
            r:setlen(d)
        end
    end
end


gerar_senha(3,4,0,B'')

-- time lua5.3 senha.lua 

-- real    9m0.402s
-- user    9m0.388s
-- sys     0m0.008s
