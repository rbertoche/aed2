# coding: utf8

# def busca(lista, n, x):
#     # Supõe X pretence a L[1..N]
#     # L[retorno] = x
#     if lista[n] == x:
#         return n
#     else:
#         return busca(lista, n-1, x)

def busca(lista, n, x):
    # Supõe X pretence a L[1..N]
    # L[retorno] = x
    if n == 0:
        return -1
    elif lista[n] == x:
        return n
    else:
        return busca(lista, n-1, x)

