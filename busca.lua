

function busca(L, N, x)
    if N == 0 then
        return 0
    elseif L[N] == x then
        return N
    else
        return busca(L, N-1, x)
    end
end