

-- s table, e table, N inteiro, D inteiro, R table de lógicos
-- s -> start
-- e -> end
-- N -> tamanho
-- d ->
-- R -> usados
-- cmax ->
function sma_bk(s, e, N, d, R, cmax)
    if d == N then
        local c = 0
        for i=1, N do
            if R[i] then
                c = c+1
            end
        end
        if cmax[1] < c then
            cmax[1] = c
        end
    else
        R[d+1] = false
        sma_bk(s, e, N, d+1, R, cmax)
        local posso = true
        for i = 1, d do
            if R[i] and e[i] > s[d+1]
                    and e[d+1] > s[i] then
                posso = false
                break
            end
            if posso then
                R[d+1] = true
                sma_bk(s, e, N, d+1, R, cmax)
            end
        end
    end
end

function S(s, e, N, i, j)
    if T[i][j] == nil then
        local cmax = 0
        for z = 1, N do
            if e[i] <= s[z] and e[z] <= s[j] then
                c = S(s, e, N, i, z) +
                    S(s, e, N, i, z) + 1
                if cmax < c then
                    cmax = c
                end
            end
        end
        T[i][j] = cmax
    end
    return T[i][j]
end

function sma_guloso(s, e, N, i, j)
    -- assume e ordenado
    for z = i+1, j-1 do
        if e[i] <= s[z] and
           e[z] <= s[j] then
            -- z é o z*!
            return 1 + sma_guloso(s, e, N, z, j)
        end
    end
    return 0
end


local N = 5

T = {}
for i = 0, 20 do
    T[i] = {}
end

local s = {0, 1, 4, 8,  6}
local e = {3, 5, 6, 10, 12}


local i = 1
local j = 1

--local cmax = {-1}
--sma_bk(s, e, N, 0, {}, cmax)
--print(cmax[1])

--print(S(s, e, 5, 1, 5))

print(sma_guloso(s, e, 5, i, j))


